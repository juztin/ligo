# Quick Start

```sh
yarn start
open http://localhost:8080
```

# Available Scripts

In the project directory, you can run:

## `yarn start`

Runs the server in development mode. This will also start the client.

## `yarn test`

Runs tests.

## `yarn build`

Builds the application for production to the `dist` folder.
