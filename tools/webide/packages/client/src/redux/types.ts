export enum Language {
  PascaLigo = 'pascaligo',
  CameLigo = 'cameligo',
  ReasonLIGO = 'reasonligo'
}

export enum Command {
  Compile = 'compile',
  DryRun = 'dry-run',
  EvaluateValue = 'evaluate-value',
  EvaluateFunction = 'evaluate-function',
  Deploy = 'deploy'
}
