# TODO: remove this as submodules aren't used anymore.
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  build_binary_script: "./scripts/distribution/generic/build.sh"
  package_binary_script: "./scripts/distribution/generic/package.sh"
  LIGO_REGISTRY_IMAGE_BASE_NAME: "${CI_PROJECT_PATH}/${CI_PROJECT_NAME}"

stages:
  - test
  - ide
  - build_and_package_binaries
  - build_docker
  - build_and_deploy_docker
  - build_and_deploy_website

# TODO provide sensible CI for master
dont-merge-to-master:
  stage: test
  script:
    - "false"
  only:
    - master

.build_binary: &build_binary
  # To run in sequence and save CPU usage, use stage: build_and_package_binaries
  stage: test
  script:
    - $build_binary_script "$target_os_family" "$target_os" "$target_os_version"
    - $package_binary_script "$target_os_family" "$target_os" "$target_os_version"
  artifacts:
    paths:
      - dist/package/**/*

.website_build: &website_build
  stage: build_and_deploy_website
  image: node:8
  dependencies:
    - build-and-package-debian-9
    - build-and-package-debian-10
    - build-and-package-ubuntu-18-04
    - build-and-package-ubuntu-19-04
  before_script:
    - scripts/install_native_dependencies.sh
    - scripts/install_opam.sh # TODO: or scripts/install_build_environment.sh ?
    - export PATH="/usr/local/bin${PATH:+:}${PATH:-}"
    - eval $(opam config env)
    - scripts/setup_switch.sh
    - eval $(opam config env)
    - scripts/setup_repos.sh

    # install deps for internal documentation
    - scripts/install_vendors_deps.sh
    - opam install -y odoc
    - scripts/build_ligo_local.sh

    # build with odoc
    - dune build @doc

    # copy .deb packages into website
    - find dist -name \*.deb -exec sh -c 'cp {} gitlab-pages/website/static/deb/ligo_$(basename $(dirname {})).deb' \;

    # npm
    - cd gitlab-pages/website
    - npm install
  script:
    - npm run version next
    - npm run build
    # move internal odoc documentation to the website folder
    - mkdir -p build/ligo/
    - mv ../../_build/default/_doc/_html/ build/ligo/odoc
    - pwd            # for debug
    - ls build/ligo/ # for debug
  after_script:
    - cp -r gitlab-pages/website/build/ligo public
  artifacts:
    paths:
      - public

.docker: &docker
  image: docker:19
  services:
    - docker:19-dind


.before_script: &before_script
  before_script:
    # Install dependencies
    # rsync is needed by opam to sync a package installed from a local directory with the copy in ~/.opam
    - scripts/install_native_dependencies.sh
    - scripts/install_opam.sh # TODO: or scripts/install_build_environment.sh ?
    - export PATH="/usr/local/bin${PATH:+:}${PATH:-}"
    - eval $(opam config env)
    - scripts/setup_switch.sh
    - eval $(opam config env)
    - scripts/setup_repos.sh

local-dune-job:
  <<: *before_script
  stage: test
  script:
    - scripts/install_vendors_deps.sh
    - scripts/build_ligo_local.sh
    - dune runtest
    - make coverage
  artifacts:
    paths:
      - _coverage_all
  only:
    - merge_requests
    - dev

# Run a docker build without publishing to the registry
build-current-docker-image:
  stage: build_docker
  dependencies:
    - build-and-package-debian-10
  <<: *docker
  script:
    - sh scripts/build_docker_image.sh
    - sh scripts/test_cli.sh
  only:
    - merge_requests

# When a MR/PR is merged to dev
# take the previous build and publish it to Docker Hub
build-and-publish-latest-docker-image:
  stage: build_and_deploy_docker
  <<: *docker
  dependencies:
    - build-and-package-debian-10
  script:
    - sh scripts/build_docker_image.sh
    - sh scripts/test_cli.sh
    - docker login -u $LIGO_REGISTRY_USER -p $LIGO_REGISTRY_PASSWORD
    - docker push ${LIGO_REGISTRY_IMAGE_BUILD:-ligolang/ligo}:next
  only:
    - dev

# It'd be a good idea to generate those jobs dynamically,
# based on desired targets
build-and-package-debian-9:
  <<: *docker
  # To run in sequence and save CPU usage, use stage: build_and_package_binaries
  stage: test
  variables:
    target_os_family: "debian"
    target_os: "debian"
    target_os_version: "9"
  <<: *build_binary
  only:
    - dev

build-and-package-debian-10:
  <<: *docker
  # To run in sequence and save CPU usage, use stage: build_and_package_binaries
  stage: test
  variables:
    target_os_family: "debian"
    target_os: "debian"
    target_os_version: "10"
  <<: *build_binary
  # this one is merge_requests and dev, because the debian 10 binary
  # is used for build-current-docker-image and for
  # build-and-publish-latest-docker-image
  only:
    - merge_requests
    - dev

build-and-package-ubuntu-18-04:
  <<: *docker
  # To run in sequence and save CPU usage, use stage: build_and_package_binaries
  stage: test
  variables:
    target_os_family: "debian"
    target_os: "ubuntu"
    target_os_version: "18.04"
  <<: *build_binary
  only:
    - dev

build-and-package-ubuntu-19-04:
  <<: *docker
  # To run in sequence and save CPU usage, use stage: build_and_package_binaries
  stage: test
  variables:
    target_os_family: "debian"
    target_os: "ubuntu"
    target_os_version: "19.04"
  <<: *build_binary
  only:
    - dev


trigger-webide:
  stage: ide
  trigger:
    include: tools/webide/webide-ci.yml


# Pages are deployed from dev, be careful not to override 'next'
# in case something gets merged into 'dev' while releasing.
pages:
  <<: *website_build
  only:
    - dev
