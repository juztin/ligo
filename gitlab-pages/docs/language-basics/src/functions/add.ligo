function add(const a: int; const b: int): int is 
    begin
        const result: int = a + b;
    end with result;