const increment : (int -> int) = (function (const i : int) : int is i + 1);
// a = 2
const a: int = increment(1);