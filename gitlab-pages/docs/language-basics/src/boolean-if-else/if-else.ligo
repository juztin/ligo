const min_age: nat = 16n;

function is_adult(const age: nat): bool is
    if (age > min_age) then True else False