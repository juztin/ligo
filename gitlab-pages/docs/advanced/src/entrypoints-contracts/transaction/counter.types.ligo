type action is
| Increment of int
| Decrement of int
| Reset of unit