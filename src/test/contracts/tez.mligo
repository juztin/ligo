let add_tez : tez = 21mutez + 0.000021tz
let sub_tez : tez = 0.000021tz - 0.000020tz
let not_enough_tez : tez = 4611686018427.387903tz

let add_more_tez : tez = 100tz + 10tz + 1tz + 0.1tz + 0.01tz + 0.001tz
