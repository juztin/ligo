;; Build of the lexer

(ocamllex LexToken)

;; Build of the parser

(menhir
  (merge_into Parser)
  (modules ParToken Parser)
  (flags -la 1 --table --strict --explain --external-tokens LexToken))

;; Build of the parser as a library

(library
  (name parser_pascaligo)
  (public_name ligo.parser.pascaligo)
  (modules
     Scoping AST pascaligo Parser ParserLog LexToken ParErr)
  (libraries
     menhirLib
     parser_shared
     hex
     simple-utils)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Parser_shared -open Simple_utils)))

;; Build of the unlexer (for covering the
;; error states of the LR automaton)

(executable
  (name Unlexer)
  (libraries str)
  (preprocess
    (pps bisect_ppx --conditional))
  (modules Unlexer))

;; Local build of a standalone lexer

(executable
  (name LexerMain)
  (libraries
     hex simple-utils tezos-utils parser_pascaligo)
  (modules LexerMain)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Parser_shared -open Parser_pascaligo)))

;; Local build of a standalone parser

(executable
  (name ParserMain)
  (libraries parser_pascaligo)
  (modules ParserMain)
  (preprocess
    (pps bisect_ppx --conditional))
  (flags (:standard -open Simple_utils -open Parser_shared -open Parser_pascaligo)))

;; Build of the covering of error states in the LR automaton

(rule
  (targets Parser.msg)
  (deps (:script_messages ../../../../vendors/ligo-utils/simple-utils/messages.sh) Parser.mly LexToken.mli ParToken.mly)
  (action (run %{script_messages} --lex-tokens=LexToken.mli --par-tokens=ParToken.mly Parser.mly)))

;; Build of all the LIGO source file that cover all error states

(rule
  (targets all.ligo)
  (deps (:script_cover ../../../../vendors/ligo-utils/simple-utils/cover.sh) Parser.mly LexToken.mli ParToken.mly Parser.msg Unlexer.exe)
  (action (run %{script_cover} --lex-tokens=LexToken.mli --par-tokens=ParToken.mly --ext=ligo --unlexer=./Unlexer.exe --messages=Parser.msg --dir=. --concatenate Parser.mly)))

;; Error messages

;; Generate error messages from scratch
; (rule
;  (targets error.messages)
;  (deps Parser.mly ParToken.mly error.messages.checked-in)
;  (action
;   (with-stdout-to %{targets} 
;   (bash 
;     "menhir \
;       --unused-tokens \
;       --list-errors  \
;       --table  \
;       --strict  \
;       --external-tokens LexToken.mli  \
;       --base Parser.mly  \
;       ParToken.mly  \
;       Parser.mly
;     "
;     )
;  ))
; )

(rule
 (targets error.messages)
 (deps Parser.mly ParToken.mly error.messages.checked-in LexToken.mli)
 (action
  (with-stdout-to %{targets} 
  (run 
    menhir
      --unused-tokens
      --update-errors error.messages.checked-in 
      --table
      --strict
      --external-tokens LexToken.mli
      --base Parser.mly
      ParToken.mly
      Parser.mly
    )
 ))
)

(rule
  (target error.messages.new)
  (action
    (with-stdout-to %{target} 
      (run 
        menhir
          --unused-tokens
          --list-errors
          --table
          --strict
          --external-tokens LexToken.mli
          --base Parser.mly
          ParToken.mly
          Parser.mly
      )
    )
  )
)

(alias
  (name runtest)
  (deps error.messages error.messages.new)
  (action
    (run 
      menhir
        --unused-tokens
        --table
        --strict
        --external-tokens LexToken.mli
        --base Parser.mly
        ParToken.mly
        Parser.mly
        --compare-errors error.messages.new
        --compare-errors error.messages
      )
    )
  )



(rule
 (targets ParErr.ml)
 (deps Parser.mly ParToken.mly error.messages.checked-in LexToken.mli)
 (action
  (with-stdout-to %{targets} 
  (run 
    menhir
      --unused-tokens
      --table
      --strict
      --external-tokens LexToken.mli
      --base Parser.mly
      ParToken.mly
      Parser.mly
      --compile-errors error.messages.checked-in
    )
 ))
)

